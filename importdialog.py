# Copyright 2014 Francesco Marano
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

__author__ = "Francesco Marano <francesco.mrn24@gmail.com>"
__copyright__ = "Copyright (C) 2014 Francesco Marano"
__version__ = "1.0.1"


from PyQt5.QtCore import QCoreApplication
from PyQt5.QtWidgets import QDialog
from tgzarchive import TGZArchive
from zarchive import ZArchive
import converter
import os

from ui.Ui_importdialog import Ui_ImportDialog


class ImportDialog(QDialog):
    """Class which implement the main logic of all the program."""
    def __init__(self,  archivesUri):
        """
        Instantiate a new import dialog whit given urls as arguments which will be validated
        to be sure they are valid tgz archive files' urls.
        """
        super(ImportDialog, self).__init__()
        self.ui = Ui_ImportDialog()
        self.ui.setupUi(self)
        self.archivesUri = self.__validateURIs(archivesUri)
        self.__logProgress("<b><i>Found %d valid archives</i></b>." % len(self.archivesUri), newline=True)
        # check if there is at least one archive to work on
        if not self.archivesUri:
            self.ui.chooseButtonBox.accepted.connect(self.close)
        else:
            self.ui.chooseButtonBox.accepted.connect(self.doInBackground)
        self.ui.chooseButtonBox.rejected.connect(self.close)
        self.ui.progressBar.setMaximum(len(self.archivesUri))

    def __validateURIs(self,  URIs):
        """Given a list of uris, returns the sublist of valid tgz archive files' URIs"""
        validURIs = []
        for uri in URIs:
            if TGZArchive.isvalid(uri):
                validURIs.append(uri)
            else:
                self.__logProgress("<span style='color: red;'><b>%s</b> was not accepted</span>" % os.path.basename(uri))
        return validURIs

    def __logProgress(self,  text='', newline=False):
        """Show a textual feedback on the UI for the user"""
        if newline:
            text += "<br/><br/>"
        self.ui.progressText.append(text)
        QCoreApplication.processEvents()

    def doInBackground(self):
        """Method to handle the importing and manipulating execution flow"""
        self.ui.chooseButtonBox.setVisible(False)
        for uri in self.archivesUri:
            # check for uri (and corresponding file) correctness and
            # open archive to keep it ready to extract members
            self.__logProgress("<b>Opening archive:</b> <i>%s</i>" % os.path.basename(uri))
            with TGZArchive(uri) as tgz:
                self.__logProgress("<b>::</b> extracted correctly")
                # search for all the components.Z archives contained in the .tgz archive (should be 2)
                members = tgz.searchfor("components.Z")
                self.__logProgress("<b>::</b> found %d <i>components.Z</i> archives" % len(members))
                # manipulate one by one the components.Z archive
                for i, member in enumerate(members, start=1):
                    z = ZArchive(member)
                    z.uncompress()
                    converter.convert(z.path())
                    z.compress()
                    self.__logProgress("<b>&nbsp;&nbsp;-></b> %d of %d converted" % (i,  len(members)))
                self.__logProgress("<b>::</b> compressing archive <i>(this may take some time...)</i>")
            self.ui.progressBar.setValue(self.ui.progressBar.value() + 1)
            self.__logProgress("<b>Done:</b> %s" % tgz.originfilename, newline=True)
        self.__logProgress("<b>All done!</b>")
