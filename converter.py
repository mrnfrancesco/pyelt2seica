# Copyright 2014 Francesco Marano
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

__author__ = "Francesco Marano <francesco.mrn24@gmail.com>"
__copyright__ = "Copyright (C) 2014 Francesco Marano"
__version__ = "1.0.1"

"""
Module used as a converter from elt to seica format for components
description written using ODB++ standard.
"""


import re
from decimal import Decimal


def convert(filepath):
    """
    Open the specified file (which is expected to be the path to a components
    file) and convert all the occurrence of the cases shown below and overwrite
    input file.
    N.B.: there will be no confirmation before overwriting

    List of handled cases for different properties values possible formats:
        * x.y[ ][mm]
        * x,y[ ][mm]
        * x[ ][mm]

    N.B.: 'Altezza_Comp' property has greater priority than 'height'. If none of
            these properties was given for a row, no matching (and replacing) will
            be performed and the unmatching string will be copied as it is.
    """
    with open(filepath, 'r') as finput:
        # case of 'Altezza_Comp' present in component description
        HACKED = re.sub(
            pattern=r"(?P<comp>CMP.*,\d+=)(\d+(\.\d+)?)(?P<prop>\nPRP\ Altezza_Comp\ \'(?P<value>\d+(,|\.)?\d*)((\ )?mm)?\')",
            repl=__format,
            string=finput.read()
        )
        # case of 'Altezza_Comp' missing, but 'height' present
        HACKED = re.sub(
            pattern=r"(?P<comp>CMP.*,\d+=)(\d+(\.\d+)?)(?P<junk>\nPRP\ Value.*\n)(?P<prop>PRP\ height\ \'(?P<value>\d+(,|\.)?\d*)(\ mm)?\')",
            repl=__format,
            string=HACKED
        )
    with open(filepath, 'w') as foutput:
        foutput.write(HACKED)

def __format(matchobj):
    FORMATTED = matchobj.group('comp')
    FORMATTED += str(__mm2inch(matchobj.group('value')))
    try:
        FORMATTED += matchobj.group('junk')
    except IndexError:
        pass
    FORMATTED += matchobj.group('prop')
    return FORMATTED

def __mm2inch(measure):
    return Decimal(measure.replace(",", ".")) * Decimal('.03937')
