# Copyright 2014 Francesco Marano
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

__author__ = "Francesco Marano <francesco.mrn24@gmail.com>"
__copyright__ = "Copyright (C) 2014 Francesco Marano"
__version__ = "1.0.1"


import os
import shutil
import tarfile
import tempfile


class TGZArchive:
    """
    Class used to represent GZIP Compressed Tar archives (*.tar.gz or *.tgz)
    and do some very basic stuff with them like compress, uncompress and search
    into the archive for a specific filename.
    """
    @staticmethod
    def isvalid(uri):
        """
        Checks if the specified path points to a valid tgz archive.
        Returns true if it is valid, false otherwise.
        No methods are provided except for 'searchfor(filename)' because all
        the extraction and compression is fully automatical.

        example of usage:

        with TGZArchive('a/valid/uri/to/valid/archive.tgz') as tgz:
            ...
            members = tgz.searchfor('components.Z')
            ... implement your logic here ...
        """
        return os.path.isfile(uri) and tarfile.is_tarfile(uri)

    def __init__(self, path):
        """
        Create an object which represent the specified tgz compressed archive.
        If the specified path do not point to a valid tgz archive a TarError
        is raised; otherwise a temporary directory to works with archive is
        created.
        """
        path = os.path.normpath(path)  # normalize path
        # If file does not exists, is a directory or is a device
        # an error occurred and will be re-raised.
        # If specified file is not a tar file a TarError is raised.
        if not TGZArchive.isvalid(path):
            raise tarfile.TarError('Not a GZIP Compressed Tar archive: ' + os.path.basename(path))
        # Stores information on input directory
        self.origindir = os.path.dirname(path)
        self.originfilename = os.path.basename(path)
        # Stores information on output filename and directory
        self.workingdir = tempfile.mkdtemp()
        self.workingfilename = "%s_parsed" % os.path.splitext(self.originfilename)[0]

    def __originpath(self):
        """Returns the absolute path of the input archive"""
        return os.path.join(self.origindir,  self.originfilename)

    def __resultpath(self, extension='tar.gz'):
        """Returns the absolute path of the output archive"""
        resultfilename = ".".join([self.workingfilename, extension])
        return os.path.join(self.origindir,  resultfilename)

    def __enter__(self):
        """Extract all the archive content into temporary working directory"""
        with tarfile.open(self.__originpath()) as tar:
            tar.extractall(path=self.workingdir)
            return self

    def __exit__(self, type, value, traceback):
        """
        Re-compresses all the archive content in a tgz archive and removes
        the working directory and all its content.
        """
        self.__compress()
        shutil.move(self.__resultpath(), self.__resultpath(extension='tgz'))
        # Removes temporary directory used to extract archive
        shutil.rmtree(self.workingdir)

    def searchfor(self, filename):
        """Returns a list of absolute path to archive members with the specified filename"""
        # open file on file system referenced by specified path
        with tarfile.open(self.__originpath()) as tar:
            # search for all the file with specified filename contained in the .tgz archive
            members = [os.path.join(self.workingdir, info.name) for info in tar if os.path.basename(info.name) == filename]
        return members

    def __compress(self,  overwrite=True):
        """
        Re-compresses all the archive content of the working directory in a tgz archive.
        If overwrite is passed as False a FileExistsError is raised if the output archive
        already exists.
        """
        resultpath = self.__resultpath(extension='tar.gz')
        if not overwrite and os.path.isfile(resultpath):
            raise FileExistsError(resultpath)

        with tarfile.open(resultpath, 'w:gz') as tar:
            for memberpath in os.listdir(self.workingdir):
                tar.add(os.path.join(self.workingdir,  memberpath), arcname=memberpath, recursive=True)
