[TOC]

# Log degli errori #

## metodo grafico ##

* click con il tasto destro sull'icona del programma
* selezionare la voce ```Proprietà```
* spostarsi sulla scheda ```Avviatore```
* abilitare/disabilitare la spunta sulla voce ```Esegui nel terminale```

## metodo a riga di comando ##

* aprire la cartella che contiene i sorgenti del programma
* click con il tasto destro su un punto vuoto della finestra
* selezionare la voce ```Terminal```

Digitare quanto segue e premere ```Invio```

```
#!bash
python3 main.py

```

# Come si aggiorna #

* aprire la cartella che contiene i sorgenti del programma
* eliminare la cartella ```__pycache__``` e tutto il suo contenuto
* click con il tasto destro su un punto vuoto della finestra
* selezionare la voce ```Terminal```

Digitare quanto segue e premere ```Invio```

```
#!bash
git pull

```

# Dove segnalare problemi #

Per segnalare un problema/consiglio/miglioramento/ecc utilizzare questo [link](https://bitbucket.org/mrnfrancesco/pyelt2seica/issues/new)

Per guardare la lista delle segnalazioni utilizzare questo [link](https://bitbucket.org/mrnfrancesco/pyelt2seica/issues?status=new&status=open)