# Copyright 2014 Francesco Marano
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

__author__ = "Francesco Marano <francesco.mrn24@gmail.com>"
__copyright__ = "Copyright (C) 2014 Francesco Marano"
__version__ = "1.0.1"


import os
from PyQt5.QtCore import QProcess


class ZArchive:
    """
    Class used to represent Z compressed archives (*.Z) and do some very
    basic stuff with them like compress and uncompress.
    """
    def __init__(self, path):
        """
        Create an object which represent the Z compressed archive specified,
        which must be one of the 'components.Z' archives in the original tgz.
        If the specified path do not point to a valid Z archive a BadZFile
        error will be raised.
        """
        self.iscompressed = True
        path = os.path.normpath(path)  # normalize path
        # Stores information on input
        self.dir = os.path.dirname(path)
        self.filename = os.path.basename(path)
        # If file does not exists, is a directory or is a device
        # an error occurred and will be re-raised.
        # If specified file is not a Z file a BadZFile is raised.
        if not os.path.isfile(path):  # TODO: better validity check
            raise BadZFile('Not a Z compressed archive: ' + self.filename)

    def path(self):
        """Returns the absolute path of the compressed/uncompressed archive"""
        return os.path.join(self.dir, self.filename)

    def compress(self):
        """
        Compresses back the file previously extracted from the archive.
        If the archive is not uncompressed, the method does nothing.
        """
        # check if file is already compressed and in that
        # case do nothing
        if not self.iscompressed:
            self.__run('compress')
            self.compressed = True
            self.filename += ".Z"

    def uncompress(self):
        """
        Uncompresses the archive file extracting the 'components' file in it.
        If the archive is already uncompressed, the method does nothing.
        """
        # check if file is already uncompressed and in that
        # case do nothing
        if self.iscompressed:
            self.__run('uncompress')
            self.iscompressed = False
            self.filename = os.path.splitext(self.filename)[0]

    def __run(self, program):
        """
        Start a new blocking subprocess executing 'program' with archive path
        as parameter.
        If an error occurred, a ProcessError is raised.
        """
        p = QProcess()
        p.start(program, [self.path()])
        if not p.waitForFinished():
            raise ProcessError()


class BadZFile(Exception):
    """Base exception for non valid Z archives"""
    pass

class ProcessError(Exception):
    """Base exception for subprocess executing error"""
    pass
