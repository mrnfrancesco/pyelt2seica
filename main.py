# Copyright 2014 Francesco Marano
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

__author__ = "Francesco Marano <francesco.mrn24@gmail.com>"
__copyright__ = "Copyright (C) 2014 Francesco Marano"
__version__ = "1.0.1"


from PyQt5.QtWidgets import QApplication
from mainwindow import MainWindow
import sys

if __name__ == '__main__':
    """Program launcher"""
    app = QApplication(sys.argv)
    w = MainWindow()
    w.show()
    sys.exit(app.exec_())
