# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file '/home/francesco/git/pyelt2seica/ui/importdialog.ui'
#
# Created: Sun Jun  8 15:25:56 2014
#      by: PyQt5 UI code generator 5.2.1
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_ImportDialog(object):
    def setupUi(self, ImportDialog):
        ImportDialog.setObjectName("ImportDialog")
        ImportDialog.setWindowModality(QtCore.Qt.WindowModal)
        ImportDialog.resize(528, 299)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Preferred, QtWidgets.QSizePolicy.Preferred)
        sizePolicy.setHorizontalStretch(10)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(ImportDialog.sizePolicy().hasHeightForWidth())
        ImportDialog.setSizePolicy(sizePolicy)
        ImportDialog.setMinimumSize(QtCore.QSize(442, 210))
        ImportDialog.setLocale(QtCore.QLocale(QtCore.QLocale.English, QtCore.QLocale.UnitedStates))
        ImportDialog.setWindowFilePath("")
        ImportDialog.setSizeGripEnabled(True)
        ImportDialog.setModal(True)
        self.verticalLayout_2 = QtWidgets.QVBoxLayout(ImportDialog)
        self.verticalLayout_2.setObjectName("verticalLayout_2")
        self.verticalLayout = QtWidgets.QVBoxLayout()
        self.verticalLayout.setSizeConstraint(QtWidgets.QLayout.SetMaximumSize)
        self.verticalLayout.setContentsMargins(5, 5, 5, 5)
        self.verticalLayout.setObjectName("verticalLayout")
        self.progressBar = QtWidgets.QProgressBar(ImportDialog)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.progressBar.sizePolicy().hasHeightForWidth())
        self.progressBar.setSizePolicy(sizePolicy)
        self.progressBar.setMinimumSize(QtCore.QSize(330, 26))
        self.progressBar.setMaximum(10)
        self.progressBar.setProperty("value", 0)
        self.progressBar.setAlignment(QtCore.Qt.AlignCenter)
        self.progressBar.setTextVisible(True)
        self.progressBar.setOrientation(QtCore.Qt.Horizontal)
        self.progressBar.setInvertedAppearance(False)
        self.progressBar.setTextDirection(QtWidgets.QProgressBar.TopToBottom)
        self.progressBar.setObjectName("progressBar")
        self.verticalLayout.addWidget(self.progressBar)
        self.progressText = QtWidgets.QTextEdit(ImportDialog)
        self.progressText.setMinimumSize(QtCore.QSize(414, 150))
        self.progressText.setFrameShadow(QtWidgets.QFrame.Sunken)
        self.progressText.setVerticalScrollBarPolicy(QtCore.Qt.ScrollBarAsNeeded)
        self.progressText.setHorizontalScrollBarPolicy(QtCore.Qt.ScrollBarAlwaysOff)
        self.progressText.setUndoRedoEnabled(False)
        self.progressText.setReadOnly(True)
        self.progressText.setTextInteractionFlags(QtCore.Qt.LinksAccessibleByMouse|QtCore.Qt.TextSelectableByMouse)
        self.progressText.setObjectName("progressText")
        self.verticalLayout.addWidget(self.progressText)
        self.chooseButtonBox = QtWidgets.QDialogButtonBox(ImportDialog)
        self.chooseButtonBox.setStandardButtons(QtWidgets.QDialogButtonBox.Cancel|QtWidgets.QDialogButtonBox.Ok)
        self.chooseButtonBox.setCenterButtons(True)
        self.chooseButtonBox.setObjectName("chooseButtonBox")
        self.verticalLayout.addWidget(self.chooseButtonBox)
        self.verticalLayout_2.addLayout(self.verticalLayout)

        self.retranslateUi(ImportDialog)
        QtCore.QMetaObject.connectSlotsByName(ImportDialog)

    def retranslateUi(self, ImportDialog):
        _translate = QtCore.QCoreApplication.translate
        ImportDialog.setWindowTitle(_translate("ImportDialog", "Doing dirty work for you. Please wait..."))
        self.progressBar.setFormat(_translate("ImportDialog", "%v/%m"))
        self.progressText.setHtml(_translate("ImportDialog", "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/REC-html40/strict.dtd\">\n"
"<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css\">\n"
"p, li { white-space: pre-wrap; }\n"
"</style></head><body style=\" font-family:\'Sans Serif\'; font-size:9pt; font-weight:400; font-style:normal;\">\n"
"<p style=\"-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><br /></p></body></html>"))


if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    ImportDialog = QtWidgets.QDialog()
    ui = Ui_ImportDialog()
    ui.setupUi(ImportDialog)
    ImportDialog.show()
    sys.exit(app.exec_())

