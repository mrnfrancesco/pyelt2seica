# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file '/home/francesco/git/pyelt2seica/ui/mainwindow.ui'
#
# Created: Sun Jun  8 15:25:59 2014
#      by: PyQt5 UI code generator 5.2.1
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_MainWindow(object):
    def setupUi(self, MainWindow):
        MainWindow.setObjectName("MainWindow")
        MainWindow.resize(300, 250)
        MainWindow.setMinimumSize(QtCore.QSize(300, 250))
        MainWindow.setAcceptDrops(False)
        MainWindow.setStyleSheet("")
        MainWindow.setLocale(QtCore.QLocale(QtCore.QLocale.English, QtCore.QLocale.UnitedStates))
        self.centralWidget = QtWidgets.QWidget(MainWindow)
        self.centralWidget.setObjectName("centralWidget")
        self.verticalLayout_2 = QtWidgets.QVBoxLayout(self.centralWidget)
        self.verticalLayout_2.setObjectName("verticalLayout_2")
        self.verticalLayout = QtWidgets.QVBoxLayout()
        self.verticalLayout.setObjectName("verticalLayout")
        self.dropLabel = DropHandlerLabel(self.centralWidget)
        self.dropLabel.setEnabled(True)
        self.dropLabel.setMinimumSize(QtCore.QSize(260, 150))
        font = QtGui.QFont()
        font.setBold(False)
        font.setWeight(50)
        font.setStrikeOut(False)
        font.setKerning(True)
        self.dropLabel.setFont(font)
        self.dropLabel.setAcceptDrops(True)
        self.dropLabel.setStyleSheet("background-color: argb(50, 110, 109, 107);\n"
"border-radius: 10px;\n"
"color: rgb(72, 71, 70)")
        self.dropLabel.setLocale(QtCore.QLocale(QtCore.QLocale.English, QtCore.QLocale.UnitedStates))
        self.dropLabel.setFrameShape(QtWidgets.QFrame.StyledPanel)
        self.dropLabel.setFrameShadow(QtWidgets.QFrame.Plain)
        self.dropLabel.setTextFormat(QtCore.Qt.RichText)
        self.dropLabel.setScaledContents(False)
        self.dropLabel.setAlignment(QtCore.Qt.AlignCenter)
        self.dropLabel.setWordWrap(True)
        self.dropLabel.setTextInteractionFlags(QtCore.Qt.NoTextInteraction)
        self.dropLabel.setObjectName("dropLabel")
        self.verticalLayout.addWidget(self.dropLabel)
        self.openButton = QtWidgets.QPushButton(self.centralWidget)
        self.openButton.setMinimumSize(QtCore.QSize(260, 50))
        font = QtGui.QFont()
        font.setBold(True)
        font.setWeight(75)
        self.openButton.setFont(font)
        self.openButton.setStyleSheet("background-color: rgb(18, 110, 202);\n"
"border-color: rgb(10, 61, 111);\n"
"color: white;")
        self.openButton.setLocale(QtCore.QLocale(QtCore.QLocale.English, QtCore.QLocale.UnitedStates))
        icon = QtGui.QIcon()
        icon.addPixmap(QtGui.QPixmap("ui/images/file-extension-tgz-icon.png"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.openButton.setIcon(icon)
        self.openButton.setIconSize(QtCore.QSize(32, 32))
        self.openButton.setCheckable(False)
        self.openButton.setChecked(False)
        self.openButton.setFlat(False)
        self.openButton.setObjectName("openButton")
        self.verticalLayout.addWidget(self.openButton)
        self.verticalLayout_2.addLayout(self.verticalLayout)
        MainWindow.setCentralWidget(self.centralWidget)

        self.retranslateUi(MainWindow)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)

    def retranslateUi(self, MainWindow):
        _translate = QtCore.QCoreApplication.translate
        MainWindow.setWindowTitle(_translate("MainWindow", "Parserio by Francesco Marano"))
        self.dropLabel.setText(_translate("MainWindow", "<html><head/><body><p>Drag and drop archive(s) here<br/>or click the <span style=\" font-style:italic;\">Open</span> button</p></body></html>"))
        self.openButton.setText(_translate("MainWindow", "Open"))

from drophandlerlabel import DropHandlerLabel

if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    MainWindow = QtWidgets.QMainWindow()
    ui = Ui_MainWindow()
    ui.setupUi(MainWindow)
    MainWindow.show()
    sys.exit(app.exec_())

