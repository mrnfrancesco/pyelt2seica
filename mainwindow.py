# Copyright 2014 Francesco Marano
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

__author__ = "Francesco Marano <francesco.mrn24@gmail.com>"
__copyright__ = "Copyright (C) 2014 Francesco Marano"
__version__ = "1.0.1"


from PyQt5.QtWidgets import QMainWindow, QFileDialog
from importdialog import ImportDialog
import sys

from ui.Ui_mainwindow import Ui_MainWindow


class MainWindow(QMainWindow):
    """Program main window"""
    def __init__(self):
        super(MainWindow, self).__init__()
        self.ui = Ui_MainWindow()
        self.ui.setupUi(self)
        self.ui.openButton.clicked.connect(self.chooseFiles)
        # check if user has passed some arguments from the CLI
        # (or performing drag&drop on desktop icon) and use that
        # arguments as URIs
        if len(sys.argv) > 1:
            sys.exit(ImportDialog(sys.argv[1:]).exec_())

    def chooseFiles(self):
        """
        Method connected to open button to let user choose files from an open
        dialog with filter over file extension.
        """
        dialog = QFileDialog()
        dialog.setFileMode(QFileDialog.ExistingFiles)
        dialog.setNameFilter("GZIP Compressed Tar Archive (*.tgz)")
        dialog.setViewMode(QFileDialog.Detail)
        # if user cancel the selection of files do nothing
        if not dialog.exec_():
            return
        # otherwise go on to the next step (importing archives)
        ImportDialog(dialog.selectedFiles()).exec_()
